package com.student.dto;

import org.springframework.web.multipart.MultipartFile;

public class UserDto {
	
	private int userId;
	
	private String userName;
	
	private String firstName;
	
	private String lastName;
	
	private String email;
	
	private boolean isActive;
	
	private char userType;
	
	private Integer courseId;
	
	private String courseName;
	
	private Integer professorId;
	
	private Integer studentId;
	
	private String grade;
	
	private Integer profAssignDocId;
	
	private Integer studentAssignDocId;
	
	private String professorFirstName;
	
	private String professorLastName;
	
	private MultipartFile file;
	
	private Integer assignmentSeqId;
	
	private String overAllGrade;
	
	public String getOverAllGrade() {
		return overAllGrade;
	}

	public void setOverAllGrade(String overAllGrade) {
		this.overAllGrade = overAllGrade;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public char getUserType() {
		return userType;
	}

	public void setUserType(char userType) {
		this.userType = userType;
	}
	
	public Integer getCourseId() {
		return courseId;
	}

	public void setCourseId(Integer courseId) {
		this.courseId = courseId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	
	public Integer getProfessorId() {
		return professorId;
	}

	public void setProfessorId(Integer professorId) {
		this.professorId = professorId;
	}

	public Integer getStudentId() {
		return studentId;
	}

	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public Integer getProfAssignDocId() {
		return profAssignDocId;
	}

	public void setProfAssignDocId(Integer profAssignDocId) {
		this.profAssignDocId = profAssignDocId;
	}

	public Integer getStudentAssignDocId() {
		return studentAssignDocId;
	}

	public void setStudentAssignDocId(Integer studentAssignDocId) {
		this.studentAssignDocId = studentAssignDocId;
	}
	
	public String getProfessorFirstName() {
		return professorFirstName;
	}

	public void setProfessorFirstName(String professorFirstName) {
		this.professorFirstName = professorFirstName;
	}

	public String getProfessorLastName() {
		return professorLastName;
	}

	public void setProfessorLastName(String professorLastName) {
		this.professorLastName = professorLastName;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}
	
	public Integer getAssignmentSeqId() {
		return assignmentSeqId;
	}

	public void setAssignmentSeqId(Integer assignmentSeqId) {
		this.assignmentSeqId = assignmentSeqId;
	}

	@Override
	public String toString() {
	    try {
	        return new com.fasterxml.jackson.databind.ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(this);
	    } catch (com.fasterxml.jackson.core.JsonProcessingException e) {
	    	return "User [userId=" + userId + ", userName=" + userName + ", firstName=" + firstName + ", lastName="
					+ lastName + ", email=" + email + ", isActive=" + isActive + ", userType="
					+ userType + "]"; 
	    }
	}

	
}