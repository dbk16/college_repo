package com.student.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.student.config.MailSender;
import com.student.dao.AdminDaoImpl;
import com.student.dao.AssignmentDetailsRepository;
import com.student.dao.CourseRepository;
import com.student.dao.DocumentRepository;
import com.student.dao.UserRepository;
import com.student.dto.UserDto;
import com.student.entity.AssignmentDetails;
import com.student.entity.Course;
import com.student.entity.User;
import com.student.service.FileStorageService;

@Controller
@RequestMapping("/rest/user")
public class UserRestController {
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private AdminDaoImpl adminDao;
	
	@Autowired
	private AssignmentDetailsRepository assignmentDao;
	
	@Autowired
	private CourseRepository courseRepo;
	
	@Autowired
	private DocumentRepository docRepo;
	
	@Autowired
	private MailSender mail;
	
	@Value("${test.email}")
	private String testEmail;
	
	@Autowired
    private FileStorageService fileStorageService;
	
	@Autowired
	EntityManager em;
	
	@GetMapping("/")
    public ModelAndView home() {
		return new ModelAndView("index", "user", new User());
    }

	@PostMapping("/signUp")
	public String login(@ModelAttribute("user") User userObj, Model model) throws Exception { 
		
		List<User> users;
		try {
			List<User> user = userRepo.findByEmail(userObj.getEmail());
			if (!user.isEmpty()) {
				model.addAttribute("message", "User Email Already Exists");
				return "index";
			}
			String userName = userObj.getFirstName().charAt(0)+""+userObj.getLastName();
			users = userRepo.findByUserNameContaining(userName);
			userObj.setPassword(getNewPassword(8));
			userObj.setUserName(userObj.getFirstName().charAt(0)+""+userObj.getLastName()+(users.size()+1));
			userObj.setCreatedTime(new Timestamp(new Date().getTime()));
			model.addAttribute(userObj);
			userRepo.save(userObj);
		} catch (Exception e) {
			model.addAttribute("message", e.getLocalizedMessage().toString());
			return "index";
		}
		model.addAttribute("message", "You successfully registered");
		return "registration-status-page";
	}
	
	@PostMapping("/home")
	public String getUser(@ModelAttribute("user") User userData, Model model, @ModelAttribute("userDto") UserDto userDto, HttpSession session) throws Exception {
	
		List<User> users = new ArrayList<User>();
		List<AssignmentDetails> assignmentDetails = new ArrayList<AssignmentDetails>();
		Boolean redirect = (Boolean) session.getAttribute("redirect");
		if (redirect!=null && redirect) {
			if ((Integer)session.getAttribute("userId")!=null)
				users = userRepo.findUserByuserId((Integer)session.getAttribute("userId"));
			 return getUser(model, assignmentDetails, users, session);
		}
		users = userRepo.findByUserNameAndPassword(userData.getUserName(),userData.getPassword());
		return getUser(model, assignmentDetails, users, session);
	}
	
	
	@GetMapping("/activate/{userId}")
	public String activateUser(@PathVariable("userId") String userId, Model model) throws Exception {
		User user = userRepo.findByUserId(Integer.parseInt(userId));
		user.setActive(true);
		userRepo.save(user);
		Thread emailThread = new Thread(){
			public void run(){
				mail.sendSimpleMessage(user.getEmail(), "Login Credentials", "User Name : "+user.getUserName()+" Password :"+user.getPassword());
			}
		};
		emailThread.start();
		model.addAttribute("message", "User activated successfully");
		return "success";
	}
	
	@GetMapping("/delete/{userId}")
	public String deleteUser(@PathVariable("userId") String userId, Model model) throws Exception {
		adminDao.deleteUser(userId);
		model.addAttribute("message", "User deleted successfully");
		return "success";
	}
	
	
	public List<Course> getCourses() throws Exception { 
		return courseRepo.findAll();
	}
	
	
	@PostMapping("/assign")
	public String assignment(@RequestBody String payload, Model model) throws Exception {
		JSONObject studentObj = new JSONObject(payload);
		final User user = userRepo.findByUserId(Integer.parseInt(studentObj.get("userId").toString()));
		Thread emailThread = new Thread(){
			public void run(){
				mail.sendSimpleMessage(user.getEmail(), "Assignments Details", "Admin has assigned Subjects, Please login to verify them");
			}
		};
		emailThread.start();
		adminDao.assignment(payload).toString();
		model.addAttribute("message", "Assigned Subjects successfully");
		return "success";
	}
	
	
	@GetMapping("/downloadFile/{fileId}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileId, HttpServletRequest request) {
        String contentType = null;
        Resource resource = null;
        try {
        	String fileName = docRepo.findByDocumentId(Integer.parseInt(fileId)).getDocumentName();
        	resource = fileStorageService.loadFileAsResource(fileName);
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (Exception ex) {
            System.out.println("Could not determine file type.");
        }
        if(contentType == null) {
            contentType = "application/octet-stream";
        }
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }
	
	

	public String getNewPassword(int len)  { 
		System.out.println("Generating password using random() : "); 
		
		String Capital_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"; 
		String Small_chars = "abcdefghijklmnopqrstuvwxyz"; 
		String numbers = "0123456789"; 
		String symbols = "!@#$%^&*_=+-/.?<>)"; 
		String values = Capital_chars + Small_chars +  numbers + symbols; 

		Random rndm_method = new Random(); 
		char[] password = new char[len]; 

		for (int i = 0; i < len; i++) { 
			password[i] =  values.charAt(rndm_method.nextInt(values.length())); 
		} 
		System.out.println("Your new password is : "+password.toString()); 
		return password.toString(); 
	}
	
	
	@GetMapping("/testEmail")
	public void testEmail() {
		mail.sendSimpleMessage(testEmail, "Testing Email","Test Email, Ignore it");
	}
	
	private Model getUserDetails(List<AssignmentDetails> assignmentDetails, User user, Model model) {
		 
		Set<Integer> userIds = new TreeSet<Integer>();
		List<User> assignedProfessors = new ArrayList<>();
		for(AssignmentDetails assignmentDetail: assignmentDetails) {
			userIds.add(assignmentDetail.getAssignmentId().getStudentId());
		}
		Set<Integer> professorIds = new TreeSet<Integer>();
		for(AssignmentDetails assignmentDetail: assignmentDetails) {
			professorIds.add(assignmentDetail.getAssignmentId().getProfessorId());
		}
		userIds.add(user.getUserId());
		List<User> userList = userRepo.findStudents(userIds);
		if (professorIds!=null && !professorIds.isEmpty())
			assignedProfessors = userRepo.findStudents(professorIds);
		List<Course> courses = courseRepo.findAll();
		List<UserDto> userDtoList = new ArrayList<UserDto>();
		for (Course c : courses) {
			for (AssignmentDetails a : assignmentDetails) {
				if (c.getCourseId().compareTo(a.getAssignmentId().getCourseId()) == 0) {
					UserDto userDto = new UserDto();
					userDto.setCourseId(c.getCourseId());
					userDto.setCourseName(c.getCourseName());
					userDto.setProfessorId(a.getAssignmentId().getProfessorId());
					userDto.setStudentId(a.getAssignmentId().getStudentId());
					userDto.setGrade(a.getGrade());
					userDto.setProfAssignDocId(a.getProfAssignDocId());
					userDto.setStudentAssignDocId(a.getStudentAssignDocId());
					userDto.setAssignmentSeqId(a.getAssignmentId().getAssignmentSeqId());
					userDtoList.add(userDto);
				}
			}
		}
		
		for (User u : userList) {
			for (UserDto d : userDtoList) {
				if (u.getUserId().compareTo(d.getStudentId())==0) {
					d.setUserId(u.getUserId());
					d.setFirstName(u.getFirstName());
					d.setLastName(u.getLastName());
					d.setUserName(u.getUserName());
					d.setEmail(u.getEmail());
				}
			}
		}
		
		for (User u: assignedProfessors) {
			for (UserDto d : userDtoList) {
				if (u.getUserId().compareTo(d.getProfessorId())==0) {
					d.setProfessorFirstName(u.getFirstName());
					d.setProfessorLastName(u.getLastName());
				}
			}
		}
		
		Set<Integer> courseIds = new TreeSet<Integer>();
		Map<Integer, Integer> overallGrade = new HashMap() ;
		for (AssignmentDetails a: assignmentDetails) {
			courseIds.add(a.getAssignmentId().getCourseId());
		}
		
		for (Integer s: courseIds) {
			overallGrade.put(s, 0);
		}
		
		//{ 'A++': 98, 'A+': 95, 'A': 90, 'B+': 80, 'B': 70, 'C': 60, 'D': 40 };
		Map<String, Integer> grade = new HashMap();
		grade.put("A++", 98);
		grade.put("A+", 95);
		grade.put("A", 90);
		grade.put("B+", 80);
		grade.put("B", 70);
		grade.put("C", 60);
		grade.put("D", 40);
		
		Map<Integer, Integer> NoOfCourseAssignments= new HashMap();
		
		for (AssignmentDetails a: assignmentDetails) {
			if (a.getAssignmentId().getCourseId()!=null && courseIds.contains(a.getAssignmentId().getCourseId()) && a.getGrade()!=null && grade.get(a.getGrade())!=null) {
				overallGrade.put(a.getAssignmentId().getCourseId(), overallGrade.get(a.getAssignmentId().getCourseId()) + 
						grade.get(a.getGrade()));
				
				if (NoOfCourseAssignments.get(a.getAssignmentId().getCourseId())==null) 
						NoOfCourseAssignments.put(a.getAssignmentId().getCourseId(), 0);
					NoOfCourseAssignments.put(a.getAssignmentId().getCourseId(), NoOfCourseAssignments.get(a.getAssignmentId().getCourseId()) + 1);
			}
		}
		
		Iterator<Integer> itr = NoOfCourseAssignments.keySet().iterator();
		while (itr.hasNext()) {
			Integer i = itr.next();
			overallGrade.put((Integer) i, overallGrade.get(i)/NoOfCourseAssignments.get(i));
		}
		
		Map<Integer, String> g = new HashMap<Integer, String>();
		Iterator<Integer> iter = NoOfCourseAssignments.keySet().iterator();
		while (iter.hasNext()) {
			Integer i = iter.next();
			if (overallGrade.get(i)>=grade.get("A++")) 
				g.put(i, "A++");
			else if (overallGrade.get(i)>=grade.get("A+"))
				g.put(i, "A+");
			else if (overallGrade.get(i)>=grade.get("A"))
				g.put(i, "A");
			else if (overallGrade.get(i)>=grade.get("B+"))
				g.put(i, "B+");
			else if (overallGrade.get(i)>=grade.get("B"))
				g.put(i, "B");
			else if (overallGrade.get(i)>=grade.get("C"))
				g.put(i, "C");
			else if (overallGrade.get(i)>=grade.get("D"))
				g.put(i, "D");
		}
		
		for (UserDto u: userDtoList) {
			if (g.containsKey(u.getCourseId()) ) {
				u.setOverAllGrade(g.get(u.getCourseId()));
			}
		}
		
		return model.addAttribute("userDtoList", userDtoList);
	}
	
	@GetMapping("/logout")
	public String logout(HttpSession session) {
		session.invalidate();
		return "redirect:/rest/user/"; 
	}
	
	public String getUser(Model model, List<AssignmentDetails> assignmentDetails, List<User> users, HttpSession session) {
	
		User user = users.get(0);
		if (users.size() == 0) {
			model.addAttribute("errorMessage", "UserName or Passowrd doesnt match");
			return "index";
		}
		if (users.get(0).getUserType() == 'A') {
			model.addAttribute("users", userRepo.findUserByType('A', false));
			model.addAttribute("students", userRepo.findStudents('S', true));
			model.addAttribute("courses", courseRepo.findAll());
			model.addAttribute("professors", userRepo.findProfessors('P', true));
			session.setAttribute("userId", user.getUserId());
			session.setAttribute("redirect", true);
			return "admin";
		}

		if (users.get(0).getUserType() == 'P') {
			assignmentDetails = assignmentDao.findByAssignmentIdProfessorId(user.getUserId());
			model = getUserDetails(assignmentDetails, user, model);
			session.setAttribute("redirect", true);
			session.setAttribute("userId", user.getUserId());
			return "professor";
		}

		if (users.get(0).getUserType() == 'S') {
			assignmentDetails = assignmentDao.findByAssignmentIdStudentId(user.getUserId());
			session.setAttribute("redirect", true);
			session.setAttribute("userId", user.getUserId());
			model = getUserDetails(assignmentDetails, user, model);
			
			return "student";
		}
		return "";
	}
	
	
	
}

