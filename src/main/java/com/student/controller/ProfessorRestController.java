package com.student.controller;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.student.config.MailSender;
import com.student.dao.AdminDaoImpl;
import com.student.dao.AssignmentDetailsRepository;
import com.student.dao.DocumentRepository;
import com.student.dao.UserRepository;
import com.student.dto.UserDto;
import com.student.entity.AssignmentDetails;
import com.student.entity.AssignmentId;
import com.student.entity.Document;
import com.student.entity.User;
import com.student.service.FileStorageService;

@Controller
@RequestMapping("/rest/prof")
public class ProfessorRestController {
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private DocumentRepository docRepo;
	
	@Autowired
	private AdminDaoImpl adminDao;
	
	@Autowired
	private MailSender mail;
	
	@Value("${test.email}")
	private String testEmail;

	@Autowired
    private FileStorageService fileStorageService;
	
	@Autowired
	private AssignmentDetailsRepository assignmentDao;
	
	@Autowired
	EntityManager em;
	
	@RequestMapping(value = {"/{professorId}/getAssignments"}, method = RequestMethod.GET)
	public List<AssignmentDetails> getStudents(@PathVariable("professorId") int professorId) {
		
		return assignmentDao.findByAssignmentIdProfessorId(professorId);
	}
	
	@GetMapping("/addAssignment/{courseId}/{studentId}/{professorId}/{assignmentSeqId}")
	public ModelAndView addAssignment(@PathVariable("courseId") Integer courseId, @PathVariable("studentId") Integer studentId, @PathVariable("professorId") Integer professorId, @PathVariable("assignmentSeqId") Integer assignmentSeqId) {
		AssignmentDetails assignmentDetails = new AssignmentDetails(); 
		AssignmentId assignmentId = new AssignmentId();
		assignmentId.setCourseId(courseId);
		assignmentId.setStudentId(studentId);
		assignmentId.setProfessorId(professorId);
		assignmentDetails.setLastModifiedTime(new Timestamp(System.currentTimeMillis()));
		assignmentDetails.setAssignmentId(assignmentId);
		Integer i = assignmentSeqId+1;
		assignmentDetails.getAssignmentId().setAssignmentSeqId(i);
		adminDao.addAssignment(assignmentDetails); 
		return new ModelAndView("success", "message", "Assignment added successfully");
	}
	
	@RequestMapping(value = {"/assignment"}, method = RequestMethod.POST)
	@Transactional
	public String assignment(@RequestParam("file" ) MultipartFile file, @ModelAttribute("userDto") UserDto userDto, Model model, HttpServletRequest req) throws Exception { 
		
			if (!file.isEmpty()) {
				try {
					String fileName = fileStorageService.storeFile(file);
					Document doc = new Document();
					doc.setDocumentName(fileName);
					doc.setUploadedTime(new Timestamp(new Date().getTime()));
					doc = docRepo.save(doc);
		
					Integer studentId = Integer.parseInt(req.getParameter("studentId"));
		
					UserDto assignObj = new UserDto();
					assignObj.setProfessorId(Integer.parseInt(req.getParameter("professorId")));
					assignObj.setCourseId(Integer.parseInt(req.getParameter("courseId")));
					assignObj.setStudentId(studentId);
					assignObj.setAssignmentSeqId(Integer.parseInt(req.getParameter("assignmentSeqId")));
					final User userObj = userRepo.findByUserId(studentId);
					adminDao.updateProfAssignment(doc.getDocumentId(), assignObj);
		
						Thread emailThread = new Thread(){
							public void run(){
								mail.sendSimpleMessage(userObj.getEmail(), "Assignments Details", "professor has assigned subject assignment, Please login to view and download the file");
							}
						};
						emailThread.start();
			} catch (Exception e) {
				model.addAttribute("message", "Error while submitting document"); 
				return ("success");
				}
			model.addAttribute("message", "Document submitted successfully"); 
			return ("success");
		}
			//update grade
			else {
				AssignmentDetails assignmentDetails = new AssignmentDetails();
				AssignmentId assignmentId = new AssignmentId();
				assignmentDetails.setAssignmentId(assignmentId);
				assignmentDetails.getAssignmentId().setProfessorId(Integer.valueOf(req.getParameter("professorId")));
				assignmentDetails.getAssignmentId().setCourseId(Integer.parseInt(req.getParameter("courseId")));
				assignmentDetails.getAssignmentId().setStudentId(Integer.parseInt(req.getParameter("studentId")));
				assignmentDetails.getAssignmentId().setAssignmentSeqId(Integer.parseInt(req.getParameter("assignmentSeqId")));
				if (req.getParameter("profAssignDocId")!=null && !req.getParameter("profAssignDocId").isEmpty())
					assignmentDetails.setProfAssignDocId(Integer.parseInt(req.getParameter("profAssignDocId")));
				if (req.getParameter("studentAssignDocId")!=null  && !req.getParameter("studentAssignDocId").isEmpty())
					assignmentDetails.setStudentAssignDocId(Integer.parseInt(req.getParameter("studentAssignDocId")));
				assignmentDetails.setGrade(req.getParameter("grade"));
				assignmentDetails.setLastModifiedTime(new Timestamp(System.currentTimeMillis()));
				em.merge(assignmentDetails);
				model.addAttribute("message", "Grade updated"); 
				return ("success");
			}
	}

	
	
	
}

