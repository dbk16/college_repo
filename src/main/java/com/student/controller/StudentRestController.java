package com.student.controller;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.student.config.MailSender;
import com.student.dao.AdminDaoImpl;
import com.student.dao.AssignmentDetailsRepository;
import com.student.dao.DocumentRepository;
import com.student.dao.UserRepository;
import com.student.dto.UserDto;
import com.student.entity.AssignmentDetails;
import com.student.entity.Document;
import com.student.entity.User;
import com.student.service.FileStorageService;

@RestController
@RequestMapping("/rest/student")
public class StudentRestController {
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private DocumentRepository docRepo;
	
	@Autowired
	private AdminDaoImpl adminDao;
	
	@Autowired
	private MailSender mail;
	
	@Value("${test.email}")
	private String testEmail;

	@Autowired
    private FileStorageService fileStorageService;
	
	@Autowired
	private AssignmentDetailsRepository assignmentDao;
	
	@RequestMapping(value = {"/{studentId}/getAssignments"}, method = RequestMethod.GET)
	public List<AssignmentDetails> getAssignments(@PathVariable("studentId") int studentId) {
		return assignmentDao.findByAssignmentIdStudentId(studentId);
	}

	@RequestMapping(value={"/assignment"}, method = RequestMethod.POST)
	public ModelAndView assignment(@RequestParam("file" ) MultipartFile file, @ModelAttribute("userDto") UserDto userDto, HttpServletRequest req) throws Exception {
		try {

			String fileName = fileStorageService.storeFile(file);
			Document doc = new Document();
			doc.setDocumentName(fileName);
			doc.setUploadedTime(new Timestamp(new Date().getTime()));
			doc = docRepo.save(doc);

			Integer professorId = Integer.parseInt(req.getParameter("professorId"));

			UserDto assignObj = new UserDto();
			assignObj.setProfessorId(professorId);
			assignObj.setCourseId(Integer.parseInt(req.getParameter("courseId")));
			assignObj.setStudentId(Integer.parseInt(req.getParameter("studentId")));
			assignObj.setAssignmentSeqId(Integer.parseInt(req.getParameter("assignmentSeqId")));
			final User userObj = userRepo.findByUserId(professorId);
			adminDao.updateStudentAssignment(doc.getDocumentId(), assignObj);
			
			Thread emailThread = new Thread() {
				public void run() {
					mail.sendSimpleMessage(userObj.getEmail(), "Assignments Details",
							"Student has submitted subject assignment, Please login to view and download the file");
				}
			};
			emailThread.start();
		} catch (Exception e) {
			return new ModelAndView("success", "message", "Error while submitting document"); 
		}
		return new ModelAndView("success", "message", "Document submitted successfully");
	}

	
	
}

