package com.student.dao;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.student.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{

	public List<User> findAll();    
	public User findByUserId(int userId);
	public List<User> findByUserNameContaining(String userName); 
	public List<User> findByUserNameAndPassword(String userName, String password); 
	public List<User> findByEmail(String email);  
	public List<User> findByIsActive(boolean isActive);
	public void delete(User entity); 
	public void deleteByUserId(int userId);
	public void deleteAll();
	
	@Query("select u from User u where u.userType != :userType and u.isActive = :isActive")
	public List<User> findUserByType(@Param("userType") Character c, @Param("isActive") boolean isActive);
	
	@Query("select u from User u where u.userType = :userType and u.isActive = :isActive")
	public List<User> findStudents(@Param("userType") Character c, @Param("isActive") boolean isActive);
	
	@Query("select u from User u where u.userType = :userType and u.isActive = :isActive")
	public List<User> findProfessors(@Param("userType") Character c, @Param("isActive") boolean isActive);
	
	@Query("select u from User u where u.userId in ( :studentIds)")
	public List<User> findStudents(@Param("studentIds") Set<Integer> studentIds);
	
	@Query("select u from User u where u.userId = :userId")
	public List<User> findUserByuserId(@Param("userId") Integer userId);
	

}
