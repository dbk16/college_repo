package com.student.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.student.entity.Course;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long>{

	public List<Course> findAll();    
	public Course findByCourseId(int courseId);
	public List<Course> findByCourseName(String courseName); 

	public void deleteAll();

}
