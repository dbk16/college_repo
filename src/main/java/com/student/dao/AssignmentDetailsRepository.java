package com.student.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.student.entity.AssignmentDetails;
import com.student.entity.AssignmentId;

@Repository
public interface AssignmentDetailsRepository extends JpaRepository<AssignmentDetails, AssignmentId>{

	public List<AssignmentDetails> findByAssignmentIdStudentId(int studentId); 
	public List<AssignmentDetails> findByAssignmentIdProfessorId(int professorId); 
	
}
