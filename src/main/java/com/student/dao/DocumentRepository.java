package com.student.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.student.entity.Document;

@Repository
public interface DocumentRepository extends JpaRepository<Document, Long>{

	public List<Document> findAll();    
	public Document findByDocumentId(int docId);
	public List<Document> findByDocumentName(String docName); 

	public void delete(Document doc); 
	public void deleteByDocumentId(int docId);


	public void deleteAll();

}
