package com.student.dao;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.web.servlet.ModelAndView;

import com.student.dto.UserDto;
import com.student.entity.AssignmentDetails;

@Repository
public class AdminDaoImpl {

	@Autowired
	JdbcTemplate jdbcTemplate;

	private static final Logger logger = LoggerFactory.getLogger(AdminDaoImpl.class);
	
	
	public JSONObject deleteUser(String userId) {
		try{
			jdbcTemplate.update("DELETE FROM ASSIGNMENT_DETAILS WHERE STUDENT_ID = ?", new Object[] { userId });
			jdbcTemplate.update("DELETE FROM ASSIGNMENT_DETAILS WHERE PROFESSOR_ID = ?", new Object[] { userId });
			jdbcTemplate.update("DELETE FROM USERS WHERE USER_ID = ?", new Object[] { userId });
		} catch (Exception e) {
			logger.error("::Error in deleteStudent ::::"+e.getLocalizedMessage());
			return new JSONObject("{'result':'failed','exception:'"+e.getLocalizedMessage()+"'}");
		}
		return new JSONObject("{'result':'success'}");
	}
	
	public JSONObject assignment(String payload) {

		JSONObject studentObj = new JSONObject(payload);
		
		try {
			jdbcTemplate.update("DELETE FROM ASSIGNMENT_DETAILS WHERE STUDENT_ID = ?", new Object[] { studentObj.get("userId") });
			jdbcTemplate.update("INSERT INTO ASSIGNMENT_DETAILS(STUDENT_ID, COURSE_ID, PROFESSOR_ID) VALUES(?,?,?)",
				new Object[] {studentObj.get("userId"), studentObj.get("courseId1"), studentObj.get("professorId1")});
		
			jdbcTemplate.update("INSERT INTO ASSIGNMENT_DETAILS(STUDENT_ID, COURSE_ID, PROFESSOR_ID) VALUES(?,?,?)",
				new Object[] {studentObj.get("userId"), studentObj.get("courseId2"), studentObj.get("professorId2")});
		
			jdbcTemplate.update("INSERT INTO ASSIGNMENT_DETAILS(STUDENT_ID, COURSE_ID, PROFESSOR_ID) VALUES(?,?,?)",
				new Object[] {studentObj.get("userId"), studentObj.get("courseId3"), studentObj.get("professorId3")});
		
		} catch (Exception e) {
			return new JSONObject("{'result':'failed','exception:'"+e.getLocalizedMessage()+"'}");
		}
		return new JSONObject("{'result':'success'}");
	}
	
	public String addAssignment(AssignmentDetails assignmentDetails) {

		try {
		jdbcTemplate.update("INSERT INTO ASSIGNMENT_DETAILS(STUDENT_ID, COURSE_ID, PROFESSOR_ID, LAST_MODIFIED_TIME, ASSIGNMENT_SEQ_ID) VALUES(?,?,?,?,?)",
				new Object[] {assignmentDetails.getAssignmentId().getStudentId(), assignmentDetails.getAssignmentId().getCourseId(), 
						assignmentDetails.getAssignmentId().getProfessorId(), assignmentDetails.getLastModifiedTime(), assignmentDetails.getAssignmentId().getAssignmentSeqId()});
		} catch (Exception e) {
			return "Error in assigning Assignment";
		}
		return "Assignment Added";
	}
	
	
	public void updateProfAssignment(int docId,UserDto assignObj) {
		jdbcTemplate.update("UPDATE ASSIGNMENT_DETAILS SET PROF_ASSIGN_DOC_ID = ? WHERE STUDENT_ID = ? AND PROFESSOR_ID = ? AND COURSE_ID = ? AND ASSIGNMENT_SEQ_ID = ?",
					new Object[] { docId, assignObj.getStudentId(), assignObj.getProfessorId(), assignObj.getCourseId(), assignObj.getAssignmentSeqId() });
	}
	
	public void updateStudentAssignment(int docId, UserDto assignObj) {
		jdbcTemplate.update("UPDATE ASSIGNMENT_DETAILS SET STUDENT_ASSIGN_DOC_ID = ? WHERE STUDENT_ID = ? AND PROFESSOR_ID = ? AND COURSE_ID = ?  AND ASSIGNMENT_SEQ_ID = ?",
					new Object[] { docId, assignObj.getStudentId(), assignObj.getProfessorId(), assignObj.getCourseId(), assignObj.getAssignmentSeqId() });
	}
	
	public void updateGrade(AssignmentDetails assignmentDetails) {
		jdbcTemplate.update("UPDATE ASSIGNMENT_DETAILS SET STUDENT_ASSIGN_DOC_ID = ? WHERE STUDENT_ID = ? AND PROFESSOR_ID = ? AND COURSE_ID = ?  AND ASSIGNMENT_SEQ_ID = ? "
		+ "AND GRADE = ? AND LAST_MODIFIED_TIME = ?",
		new Object[] { assignmentDetails.getStudentAssignDocId(), assignmentDetails.getAssignmentId().getStudentId(), assignmentDetails.getAssignmentId().getProfessorId(), 
		assignmentDetails.getAssignmentId().getCourseId(), assignmentDetails.getAssignmentId().getAssignmentSeqId(), assignmentDetails.getGrade(), 
		assignmentDetails.getLastModifiedTime() });
	}

}
