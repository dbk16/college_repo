package com.student.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "DOCUMENTS")
public class Document {
	
	@Id @GeneratedValue
	@Column(name = "document_id")
	private int documentId;
	
	@Column(name = "document_name")
	private String documentName;
	
	@Column(name = "uploaded_time")
	private Timestamp uploadedTime;

	public int getDocumentId() {
		return documentId;
	}

	public void setDocumentId(int documentId) {
		this.documentId = documentId;
	}

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public Timestamp getUploadedTime() {
		return uploadedTime;
	}

	public void setUploadedTime(Timestamp uploadedTime) {
		this.uploadedTime = uploadedTime;
	}

	@Override
	public String toString() {
	    try {
	        return new com.fasterxml.jackson.databind.ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(this);
	    } catch (com.fasterxml.jackson.core.JsonProcessingException e) {
	    	return "Document [documentId=" + documentId + ", documentName=" + documentName + ", uploadedTime="
					+ uploadedTime + "]"; 
	    }
		
	}
	
}
