package com.student.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "ASSIGNMENT_DETAILS")
public class AssignmentDetails {
	
	@EmbeddedId
	private AssignmentId assignmentId;
	
	@Column(name = "student_assign_doc_id")
	private Integer studentAssignDocId;
	
	@Column(name = "prof_assign_doc_id")
	private Integer profAssignDocId;
	
	@Column(name = "grade")
	private String grade;
	
	@Column(name = "last_modified_time")
	private Timestamp lastModifiedTime;
	
	public AssignmentId getAssignmentId() {
		return assignmentId;
	}
	public void setAssignmentId(AssignmentId assignmentId) {
		this.assignmentId = assignmentId;
	}
	public Integer getStudentAssignDocId() {
		return studentAssignDocId;
	}
	public void setStudentAssignDocId(Integer studentAssignDocId) {
		this.studentAssignDocId = studentAssignDocId;
	}
	public Integer getProfAssignDocId() {
		return profAssignDocId;
	}
	public void setProfAssignDocId(Integer profAssignDocId) {
		this.profAssignDocId = profAssignDocId;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	public Timestamp getLastModifiedTime() {
		return lastModifiedTime;
	}
	public void setLastModifiedTime(Timestamp lastModifiedTime) {
		this.lastModifiedTime = lastModifiedTime;
	}
	
	@Override
	public String toString() {
	    try {
	        return new com.fasterxml.jackson.databind.ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(this);
	    } catch (com.fasterxml.jackson.core.JsonProcessingException e) {
	    	return "AssignmentDetails [studentAssignDocId=" + studentAssignDocId + ", profAssignDocId=" + profAssignDocId + ", grade="
				+ grade + ", lastModifiedTime=" + lastModifiedTime + "]";
	    }
	}
	
	
	
	
}


