package com.student.entity;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Id;

@Embeddable
public class AssignmentId implements Serializable {
 
	private static final long serialVersionUID = 1L;
	
	@Column(name = "assignment_seq_id")
	private Integer assignmentSeqId;
	@Column(name = "student_id")
	private Integer studentId;
	@Column(name = "course_id")
	private Integer courseId;
	@Column(name = "professor_id")
	private Integer professorId;
 
    public AssignmentId() {
    }
 
    public AssignmentId(Integer assignmentSeqId,Integer studentId, Integer courseId, Integer professorId) {
        this.assignmentSeqId = assignmentSeqId;
    	this.studentId = studentId;
        this.courseId = courseId;
        this.professorId = professorId;
    }
 
    public Integer getAssignmentSeqId() {
		return assignmentSeqId;
	}
 
    public Integer getStudentId() {
		return studentId;
	}

	public Integer getCourseId() {
		return courseId;
	}

	public Integer getProfessorId() {
		return professorId;
	}

	
	
	
	public void setAssignmentSeqId(Integer assignmentSeqId) {
		this.assignmentSeqId = assignmentSeqId;
	}

	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}

	public void setCourseId(Integer courseId) { 
		this.courseId = courseId;
	}

	public void setProfessorId(Integer professorId) {
		this.professorId = professorId;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AssignmentId)) return false;
        AssignmentId that = (AssignmentId) o;
        return Objects.equals(getAssignmentSeqId(), that.getAssignmentSeqId()) &&
        		Objects.equals(getStudentId(), that.getStudentId()) &&
                Objects.equals(getCourseId(), that.getCourseId()) &&
                Objects.equals(getProfessorId(), that.getProfessorId());
    }
 
    @Override
    public int hashCode() {
        return Objects.hash(getAssignmentSeqId(), getStudentId(), getCourseId(), getProfessorId());
    }
}
