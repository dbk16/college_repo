<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="true" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<body>
<button onclick="location.href='/rest/user/logout'">logout</button>
	<table border="2" width="70%" cellpadding="2">
		<tr>
			<th>Course</th>
			<th>Grade</th>
			<th>Professor Name</th>
			<th>Professor Assignment</th>
			<th>Upload Assignment</th>
			<th>Overall Grade</th>
		</tr>
		<c:forEach var="userDto" items="${userDtoList}">
		<form:form action="/rest/student/assignment" modelAttribute="userDto" method="post" enctype="multipart/form-data">
				<tr>
					<td>${userDto.courseName}</td>
					<td>${userDto.grade}</td>
					<td>${userDto.professorFirstName} ${userDto.professorLastName}</td>
					<td>
						<a href="<c:url value='/rest/user/downloadFile/${userDto.profAssignDocId}' />">Download Assignment</a>  
					</td>
					
					<td>
						<input type="file" name="file">
						<input type="submit" value="Submit">
					</td>
					<td>${userDto.overAllGrade} <td>
				</tr>
					<input type="hidden" name="courseId" value="${userDto.courseId}"/>
					<input type="hidden" name="professorId" value="${userDto.professorId}"/>
					<input type="hidden" name="studentId" value="${userDto.studentId}"/>
					<input type="hidden" name="profAssignDocId" value="${userDto.profAssignDocId}"/>
					<input type="hidden" name="assignmentSeqId" value="${userDto.assignmentSeqId}"/>
			</form:form>
			</c:forEach>
	</table>
</body>
</html>