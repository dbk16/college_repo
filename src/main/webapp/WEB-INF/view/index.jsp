<!DOCTYPE html>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html lang="en">

<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>

<div class="split left">
<div class="centered">
<form:form method="POST" action="/rest/user/home" modelAttribute="user">
  <section fxLayout="row wrap" fxLayoutAlign="center center">
    <mat-card fxFlex="500px" fxFlex.xs="100%">
      <mat-card-title>Login</mat-card-title>
      <mat-card-content>
        <mat-form-field>
          <form:input type="text" placeholder="User Name" path="userName" />
        </mat-form-field>
        <br />
        <mat-form-field>
          <form:input type="password" placeholder="Password" path="password" />
        </mat-form-field>
      </mat-card-content>
      <mat-card-actions align="center">
        <input type="submit" value="Login" mat-raised-button color="primary"/>
      </mat-card-actions>
    </mat-card>
  </section>
</form:form>
</div>
</div>

<div class="split right">
<div class="centered">
<form:form method="POST" action="/rest/user/signUp" modelAttribute="user">
  <section fxLayout="row wrap" fxLayoutAlign="center center">
    <mat-card fxFlex="500px" fxFlex.xs="100%">
      <mat-card-title>Sign Up</mat-card-title>
      <mat-card-content>
        <mat-form-field>
          <form:input type="text" placeholder="First Name" path="firstName" />
        </mat-form-field>
        <br />
        <mat-form-field>
          <form:input type="text" placeholder="Last Name" path="lastName" />
        </mat-form-field>
        <br />
        <mat-form-field>
          <form:input type="email" placeholder="Email" path="email" />
        </mat-form-field>
        <br />
        <mat-form-field>
          <form:input type="date" placeholder="Date of Birth" path="dateOfBirth" />
          <mat-datepicker-toggle matSuffix [for]="picker"></mat-datepicker-toggle>
          <mat-datepicker #picker></mat-datepicker>
        </mat-form-field>
        <br />
        <mat-form-field>
          <form:input type="text" placeholder="SSN" path="ssn"/>
        </mat-form-field>
        <br />
        <mat-radio-group aria-label="Type">
          <form:radiobutton path="userType" value="P"/>Professor
          <form:radiobutton path="userType" value="S"/>Student
        </mat-radio-group>
      </mat-card-content>
      <mat-card-actions align="center">
        <button mat-raised-button color="primary" onclick="signUp()">Sign Up</button>
        <button mat-raised-button onclick="reset()">Reset</button>
      </mat-card-actions>
    </mat-card>

  </section>

</form:form>
</div>
</div>

</body>

</html>