<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
var grade;
function updateGrade(e){
	this.grade=e.value;
}
</script>

<body>
<button onclick="location.href='/rest/user/logout'">logout</button>
	<table border="2" width="70%" cellpadding="2">
		<tr>
			<th>Course</th>
			<th>Student Name</th>
			<th>Grade</th>
			<th>Student Submitted Assignment</th>
			<th>Upload Assignment</th>
			<th>Add Assignment</th>
			<th>Grade</th>
		</tr>
		<c:set var="count" value="0" scope="page" />
		<c:forEach items="${userDtoList}" var="userDto" varStatus="status">
		<form:form action="/rest/prof/assignment" modelAttribute="userDto" method="post" enctype="multipart/form-data">
				<tr>
					<td>${userDto.courseName}</td>
					<td>${userDto.firstName} ${userDto.lastName}</td>
					<td><input type="text" name="grade" value="${userDto.grade}"/></td>
					<td>
						<a href="<c:url value='/rest/user/downloadFile/${userDtoList[status.index].studentAssignDocId}'/>">Download Submitted Assignment</a>  
					</td>
					<td>
						<input type="file" name="file">
						<input type="submit" value="Submit">
					</td>
					<td>
						<a href="<c:url value='/rest/prof/addAssignment/${userDtoList[status.index].courseId}/${userDtoList[status.index].studentId}/${userDtoList[status.index].professorId}/${userDtoList[status.index].assignmentSeqId}' />">Add Assignment</a> 
					</td>
					<td>
					<button type="submit" onclick="/rest/prof/update/grade?grade">Assign</button>
					</td>
				</tr>
					<input type="hidden" name="courseId" value="${userDto.courseId}"/>
					<input type="hidden" name="professorId" value="${userDto.professorId}"/>
					<input type="hidden" name="studentId" value="${userDto.studentId}"/>
					<input type="hidden" name="assignmentSeqId" value="${userDto.assignmentSeqId}"/>
					<input type="hidden" name="profAssignDocId" value="${userDto.profAssignDocId}"/>
					<input type="hidden" name="studentAssignDocId" value="${userDto.studentAssignDocId}"/>
					<input type="hidden" name="studentAssignDocId" value="${grade}"/>
		</form:form>
			</c:forEach>
	</table>
</body>
</html>
