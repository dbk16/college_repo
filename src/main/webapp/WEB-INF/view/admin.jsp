<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
var courseId1, courseId2, courseId3, professorId1, professorId2, professorId3;
var userId;
var ids= {};

function courseIds(e){
	if(e.id==1){
		courseId1 = e.value;
		return;
	}
	if(e.id==2){
		courseId2 = e.value;
		return;
	}
	if(e.id==3){
		courseId3 = e.value;
		return;
	}
}


function professorIds(e){
	if(e.id==1){
		professorId1 = e.value;
		return;
	}
	if(e.id==2){
		professorId2 = e.value;
		return;
	}
	if(e.id==3){
		professorId3 = e.value;
		return;
	}
}

function assignTask(e) {

	ids["courseId1"] = courseId1;
	ids["courseId2"] = courseId2;
	ids["courseId3"] = courseId3;
	
	ids["professorId1"] = professorId1;
	ids["professorId2"] = professorId2;
	ids["professorId3"] = professorId3;
	ids["userId"] = e;
	
	console.log("ids ", JSON.stringify(ids));

	$.ajax({
		type : "POST",
		contentType : "application/json",
		url : "assign",
		data : JSON.stringify(ids),
		dataType : 'json',
		timeout : 100000,
		success : function(data) {
			console.log("SUCCESS: ", data);
		},
		error : function(e) {
			console.log("ERROR: ", e);
		},
		done : function(e) {
			console.log("DONE");
		}
	});

}

</script>
<body>
<button onclick="location.href='/rest/user/logout'">logout</button>
<h3>All Users</h3>
	<table border="2" width="70%" cellpadding="2">
		<tr>
			<th>First Name</th>
			<th>Last Name</th>
			<th>Student / Professor</th>
			<th>Approve/Reject</th>
		</tr>
		<c:forEach var="user" items="${users}">
			<tr>
				<td>${user.firstName}</td>
				<td>${user.lastName}</td>
				<td>${user.userType eq 'P'.charAt(0) ? 'Professor' : 'Student'}</td>
				<td>
					<button onclick="location.href='/rest/user/activate/${user.userId}'">Approve</button>
				</td>
			</tr>
		</c:forEach>
	</table>

<h3>Students</h3>
	<table border="2" width="70%" cellpadding="2">
		<tr>
			<th>Student First Name</th>
			<th>Student Last Name</th>
			<th>Course</th>
			<th>Professor</th>
			<th>Assign</th>
		</tr>
		<c:forEach var="student" items="${students}" varStatus="status" >
			<c:forEach var="i" begin="1" end="3">
			<tr>
				<td>${student.firstName}</td>
				<td>${student.lastName}</td>
				<td>
				<select id="${i}" onchange="courseIds(this)">
				<option selected="selected" >Select Course</option>
			      <c:forEach var="course" items="${courses}" >
			      	<option value="${course.courseId}">${course.courseName}</option>
			      </c:forEach>
			    </select>
				</td>
				<td>
				<select id="${i}" onchange="professorIds(this)">
				<option selected="selected" value="">Select Professor</option>
			      <c:forEach var="professor" items="${professors}">
			        <option value="${professor.userId}">${professor.firstName} ${professor.lastName}</option>
			      </c:forEach>
			    </select>
				</td>
			</tr>
			</c:forEach>
			<td>
				<button type="submit" onclick="assignTask(${students[status.index].userId})">Assign</button>
			</td>
		</c:forEach>
	</table>

<h3>Students</h3>
	<table border="2" width="70%" cellpadding="2">
		<tr>
			<th>Student First Name</th>
			<th>Student Last Name</th>
			<th>Date of Birth</th>
			<th>SSN</th>
			<th>Created On</th>
			<th>Delete</th>
		</tr>
				<c:forEach var="student" items="${students}">
			<tr>
				<td>${student.firstName}</td>
				<td>${student.lastName}</td>
				<td>${student.dateOfBirth}</td>
				<td>${student.ssn}</td>
				<td>${student.createdTime}</td>
				<td>
					<button onclick="location.href='/rest/user/delete/${student.userId}'">Delete</button>
				</td>
			</tr>
		</c:forEach>
	</table>

<h3>Professors</h3>
<table border="2" width="70%" cellpadding="2">
		<tr>
			<th>Professor First Name</th>
			<th>Professor Last Name</th>
			<th>Date of Birth</th>
			<th>SSN</th>
			<th>Created On</th>
			<th>Delete</th>
		</tr>
				<c:forEach var="professor" items="${professors}">
			<tr>
				<td>${professor.firstName}</td>
				<td>${professor.lastName}</td>
				<td>${professor.dateOfBirth}</td>
				<td>${professor.ssn}</td>
				<td>${professor.createdTime}</td>
				<td>
					<button onclick="location.href='/rest/user/delete/${professor.userId}'">Delete</button>
				</td>
			</tr>
		</c:forEach>
	</table>
	
	</body>
</html>
