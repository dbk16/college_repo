<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<body>
<h3> ${message}</h3>
<div class="centered">
<form:form method="POST" action="/rest/user/home" modelAttribute="user">
        <input type="submit" value="User Home">
</form:form>
</div>

</body>
</html>