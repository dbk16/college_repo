import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from '../state/state';
import { Subscription } from 'rxjs';
import { RestService } from '../services/rest-service';
import * as AppActions from '../state/actions';
import { ShowDialogComponent } from '../show-dialog/show-dialog.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-student-page',
  templateUrl: './student-page.component.html',
  styleUrls: ['./student-page.component.scss']
})
export class StudentPageComponent implements OnInit {

  subscription: Subscription = new Subscription();
  userInfo: any;
  userName: string = '';

  assignmentObj: any;
  courseData: any;
  allUsers: any;
  applicationUrl = "";
  selectedCourse: any = '';
  distinctCourses: any;

  gradeJson = { 'A++': 98, 'A+': 95, 'A': 90, 'B+': 80, 'B': 70, 'C': 60, 'D': 40 };

  constructor(private store: Store<State>, private service: RestService, private dialogRef: MatDialog) { }


  ngOnInit() {
    this.applicationUrl = this.service.getApplicationUrl();
    this.store.dispatch(new AppActions.LoadAllUsers());
    this.subscription.add(this.store.select(st => st.userInfo).subscribe(data => {
      this.userInfo = data || {};
      console.log('--user info--', this.userInfo);
      this.getAssignments();
    })).add(this.service.getCourses().subscribe(courseData => {
      this.courseData = courseData;
    })).add(this.store.select(st => st.allUsers).subscribe(allUsers => {
      this.allUsers = allUsers;
    }));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
  getAssignments() {
    this.service.getStudentAssignments(this.userInfo.userId).subscribe(dataObj => {
      console.log('-----get students data---', dataObj);
      this.assignmentObj = dataObj;
      this.distinctCourses = new Set(dataObj.map(o => o.assignmentId.courseId));
      this.selectedCourse = Array.from(this.distinctCourses)[0];
      console.log('this.selectedCourse', this.distinctCourses, this.selectedCourse);
    })
  }
  uniqueStudentAssignments() {
    return this.assignmentObj.filter(o => o.assignmentId.courseId == this.selectedCourse);
  }
  getProfessorName(assgn) {
    console.log('---profname--', assgn);
    const user = this.allUsers.find(u => u.userId === assgn.assignmentId.professorId);
    return user.firstName + ' ' + user.lastName;
  }
  getCourseName(courseId) {
    return this.courseData.find(u => u.courseId == courseId).courseName;
  }
  overallGrade(courseId?) {
    const asignments = this.assignmentObj.filter(o => o.grade && (o.assignmentId.courseId == courseId || !courseId));
    const gradeCount = asignments.map(g => this.gradeJson[g.grade]).reduce((sum, current) => sum + current, 0);
    if (gradeCount > 0) {
      return Object.keys(this.gradeJson).filter(g => (gradeCount / asignments.length) - this.gradeJson[g] >= 0)[0];
    } else {
      return 'NA';
    }
  }

  onSubmit(event, assgn) {
    event.preventDefault();
    const id = assgn.assignmentId.studentId + '_' + assgn.assignmentId.courseId;
    const fileUpload = document.getElementById('singleFileUploadInput_' + id) as HTMLInputElement;
    var files = fileUpload.files;
    this.uploadSingleFile(files[0], assgn);
  }

  uploadSingleFile(file, assgn) {
    const _this = this;
    var formData = new FormData();
    formData.append("file", file);
    const obj = [assgn.assignmentId];
    formData.set("payload", JSON.stringify(obj));

    var xhr = new XMLHttpRequest();
    xhr.open("POST", this.service.getApplicationUrl() + "rest/student/assignment");

    xhr.onload = function () {
      console.log(xhr.responseText);
      var response = JSON.parse(xhr.responseText);
      _this.dialogRef.open(ShowDialogComponent, { data: { text: 'Successfully Submitted', title: 'Success' } });
      _this.getAssignments();
    }

    xhr.send(formData);
  }
}

