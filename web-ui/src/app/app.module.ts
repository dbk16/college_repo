import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { CustomMaterialModule } from './custom-material-module';
import { HomePageComponent } from './home-page/home-page.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { AdminPageComponent } from './admin-page/admin-page.component';
import { StudentPageComponent } from './student-page/student-page.component';
import { ProfessorPageComponent } from './professor-page/professor-page.component';
import { ShowDialogComponent } from './show-dialog/show-dialog.component';
import { HeaderComponent } from './header/header.component';
import { RoutingModule } from './routing.module';
import { StoreModule } from '@ngrx/store';
import { reducerMap } from './state/reducer';
import { EffectsModule } from '@ngrx/effects';
import { Effects } from './state/effects';
import { RestService } from './services/rest-service';
import { HttpClientModule } from '@angular/common/http';
import { MatDatepicker } from '@angular/material';
import { AlwaysAuthGuard } from './always-auth-guard';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    SignupComponent,
    LoginComponent,
    AdminPageComponent,
    StudentPageComponent,
    ProfessorPageComponent,
    HeaderComponent,
    ShowDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    RoutingModule,
    HttpClientModule,
    CustomMaterialModule,
    StoreModule.forRoot(reducerMap),
    EffectsModule.forRoot([Effects])
  ],
  entryComponents: [ShowDialogComponent],
  providers: [RestService, ShowDialogComponent, AlwaysAuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
