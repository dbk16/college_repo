import { Injectable, Inject, isDevMode } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { catchError } from 'rxjs/internal/operators/catchError';
import { Observable, Subscription } from 'rxjs';

@Injectable()
export class RestService {

    constructor(private http: HttpClient) {
        if (isDevMode()) {
            console.log('---dev Mode---');
        }
    }

    private applicationUrl = 'http://localhost:8090/clg/';

    getApplicationUrl() {
        return this.applicationUrl;
    }

    loadUser(userObj): Observable<any> {
        return this.http.post(this.applicationUrl + 'rest/user/getUser', userObj);
    }

    getUsers(): Observable<any> {
        return this.http.get(this.applicationUrl + 'rest/user/getUsers');
    }

    addUser(userObj): Observable<any> {
        return this.http.put(this.applicationUrl + 'rest/user/signUp', userObj);
    }
    activateUser(userId): Observable<any> {
        return this.http.post(this.applicationUrl + 'rest/user/activate/' + userId, {});
    }
    assignStudentCourses(studentObj): Observable<any> {
        return this.http.post(this.applicationUrl + 'rest/user/assign', studentObj);
    }

    deleteUser(userId): Observable<any> {
        return this.http.delete(this.applicationUrl + 'rest/user/delete/' + userId);
    }

    getCourses(): Observable<any> {
        return this.http.get(this.applicationUrl + 'rest/user/getCourses');
    }
    getUserAssignments(studentId): Observable<any> {
        return this.http.get(this.applicationUrl + 'rest/user/' + studentId + '/assignments');
    }

    updateGrade(assgn) {
        return this.http.post(this.applicationUrl + 'rest/prof/update/grade', assgn);
    }

    getProfessorAssignments(professorId): Observable<any> {
        return this.http.get(this.applicationUrl + 'rest/prof/' + professorId + '/getAssignments');
    }
    addnewAssignment(assignObj): Observable<any> {
        return this.http.post(this.applicationUrl + 'rest/prof/addAssignment', assignObj);
    }

    getStudentAssignments(studentId): Observable<any> {
        return this.http.get(this.applicationUrl + 'rest/student/' + studentId + '/getAssignments');
    }


}
