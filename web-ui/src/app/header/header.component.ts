import { Component, OnInit, Input } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from '../state/state';
import * as AppActions from '../state/actions';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Input('userName') userName: string;

  constructor(private store: Store<State>) { }

  ngOnInit() {
  }

  logOut() {
    localStorage.clear();
    this.store.dispatch(new AppActions.LoadedUser(null));

  }

}
