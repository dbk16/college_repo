import { Component, OnInit, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RouterStateSnapshot, ActivatedRouteSnapshot, Router, CanActivate } from '@angular/router';


import { Store } from '@ngrx/store';
import { State } from './state/state';


@Injectable()
export class AlwaysAuthGuard implements CanActivate {
  account$: Observable<any>;

  constructor(private store: Store<State>,
    private router: Router) {
    this.account$ = this.store.select(st => st.userInfo);
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<boolean> | Promise<boolean> | boolean {
    this.account$.subscribe(data => {
      console.log('--authguard data---', data);
      if (data && data.userName && Object.keys(data).length > 0) {
        return true;
      } else {
        this.router.navigateByUrl('/home');
      }
    });
    return true;
  }
}
