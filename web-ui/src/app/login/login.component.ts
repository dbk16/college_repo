import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { State } from '../state/state';
import * as AppActions from '../state/actions';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  userForm: FormGroup = new FormGroup({});


  constructor(private store: Store<State>) {
    this.userForm = new FormGroup({
      userName: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });

  }

  ngOnInit() {

  }

  getLogin() {
    console.log('--userForm--', this.userForm);
    const payload = { userName: this.userForm.value.userName, password: this.userForm.value.password };
    localStorage.setItem('session', JSON.stringify(payload));
    this.store.dispatch(new AppActions.LoadUser(payload));
  }

}
