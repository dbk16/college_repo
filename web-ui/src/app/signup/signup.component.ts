import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormGroupDirective, NgForm } from '@angular/forms';
import { Store } from '@ngrx/store';
import { State } from '../state/state';
import { MatDialog } from '@angular/material';
import { RestService } from '../services/rest-service';
import { ShowDialogComponent } from '../show-dialog/show-dialog.component';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
  providers: [RestService]
})
export class SignupComponent implements OnInit {

  @ViewChild('formDirective') private formDirective: NgForm;
  signUpForm: FormGroup = new FormGroup({});


  constructor(private service: RestService, private dialogRef: MatDialog) {
    this.signUpForm = new FormGroup({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      userType: new FormControl('', Validators.required),
      dateOfBirth: new FormControl(''),
      ssn: new FormControl('')
    });
  }

  ngOnInit() {

  }

  signUp() {
    console.log('signUpForm', this.signUpForm);
    this.service.addUser(this.signUpForm.value).subscribe(resp => {
      if (resp && resp.result === 'success') {
        this.dialogRef.open(ShowDialogComponent, { data: { text: 'User added successfully, Check your email for login credentials' } });
        this.reset();
      } else {
        this.dialogRef.open(ShowDialogComponent, { data: { text: resp.message } });
      }
    }, err => {
      this.dialogRef.open(ShowDialogComponent, { data: { text: err } });
    });
  }

  reset() {
    this.signUpForm.reset();
    this.formDirective.resetForm();
  }

}
