import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from './state/state';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import * as AppActions from './state/actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

  subscription: Subscription = new Subscription();
  userInfo: any;
  userName: string = '';
  constructor(private store: Store<State>, private route: Router) { }

  ngOnInit() {
    const sessionPayload = localStorage.getItem('session');
    setTimeout(function () { localStorage.removeItem("session"); }, 15 * 60 * 1000);
    if (sessionPayload) {
      this.store.dispatch(new AppActions.LoadUser(JSON.parse(sessionPayload)));
    }



    this.subscription.add(this.store.select(st => st.userInfo).subscribe(data => {
      this.userInfo = data;
      console.log('--user info--', this.userInfo);
      if (this.userInfo) {
        this.userName = this.userInfo.firstName + ' ' + this.userInfo.lastName;
        if (this.userInfo.userType === 'A') {
          this.route.navigate(['adminPage']);
        } else if (this.userInfo.userType === 'P') {
          this.route.navigate(['professorPage']);
        } else if (this.userInfo.userType === 'S') {
          this.route.navigate(['studentPage']);
        } else {
          this.route.navigate(['home']);
        }
      } else {
        this.userName = '';
        this.route.navigate(['home']);
      }
    }));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
