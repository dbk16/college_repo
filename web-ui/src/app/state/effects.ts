import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import * as AppActions from './actions';
import { switchMap } from 'rxjs/internal/operators/switchMap';
import { catchError } from 'rxjs/internal/operators/catchError';
import { mergeMap } from 'rxjs/internal/operators/mergeMap';
import { empty } from 'rxjs/internal/observable/empty';

import { RestService } from '../services/rest-service';
import { Observable, of } from 'rxjs';
import { MatDialog } from '@angular/material';
import { ShowDialogComponent } from '../show-dialog/show-dialog.component';


@Injectable()
export class Effects {

    constructor(private actions$: Actions, private dialogRef: MatDialog,
        private restService: RestService) {
    }


    @Effect()
    loadUser$: Observable<Actions> = this.actions$.pipe(
        ofType<AppActions.LoadUser>(AppActions.LOAD_USER),
        switchMap(action => {
            console.log('--action --', action.payload);
            return this.restService.loadUser(action.payload)
                .pipe(mergeMap(resp => {
                    if (resp && resp.result === 'success') {
                        return [new AppActions.LoadedUser(resp.message)];
                    } else {
                        this.dialogRef.open(ShowDialogComponent, { width: '500px', data: { text: resp.message } });
                    }
                }),
                    catchError((error) => this.error(error)));
        })
    );

    @Effect()
    loadAllUsers$: Observable<Actions> = this.actions$.pipe(
        ofType<AppActions.LoadAllUsers>(AppActions.LOAD_ALL_USERS),
        switchMap(action => {
            return this.restService.getUsers()
                .pipe(mergeMap(result => {
                    return [
                        new AppActions.LoadedAllUsers(result)
                    ];
                }),
                    catchError((error) => this.error(error)));
        })
    );



    private error(error): Observable<any> {
        console.log('---error in effects---', error);
        this.dialogRef.open(ShowDialogComponent, { width: '500px', data: { text: error } });
        return empty();
    }
}
