import { Action } from '@ngrx/store';

export const LOAD_USER = 'LOAD_USER';
export const LOADED_USER = 'LOADED_USER';

export const LOAD_ALL_USERS = 'LOAD_ALL_USERS';
export const LOADED_ALL_USERS = 'LOADED_ALL_USERS';

export class LoadUser implements Action {
    public readonly type = LOAD_USER;
    constructor(public payload) { }
}
export class LoadedUser implements Action {
    readonly type = LOADED_USER;
    constructor(public payload) { }
}
export class LoadAllUsers implements Action {
    readonly type = LOAD_ALL_USERS;
    constructor() { }
}
export class LoadedAllUsers implements Action {
    readonly type = LOADED_ALL_USERS;
    constructor(public payload) { }
}

export type Actions = LoadUser | LoadedUser | LoadAllUsers | LoadedAllUsers;
