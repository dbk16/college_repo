import { State } from './state';
import { ActionReducerMap } from '@ngrx/store';
import * as AppActions from '../state/actions';

export function userInfoReducer(state = null, action): any {
    switch (action.type) {
        case AppActions.LOADED_USER: {
            const act: AppActions.LoadedUser = action;
            return act.payload;
        }
        default: {
            return state;
        }
    }
}
export function allUsersReducer(state = null, action): any {
    switch (action.type) {
        case AppActions.LOADED_ALL_USERS: {
            const act: AppActions.LoadedAllUsers = action;
            return act.payload;
        }
        default: {
            return state;
        }
    }
}


export const reducerMap: ActionReducerMap<State> = {
    userInfo: userInfoReducer,
    allUsers: allUsersReducer
};
