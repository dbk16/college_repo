
export interface State {
    userInfo?: {
        userId: any;
        userName: string;
        email: string;
        firstName: string;
        lastName: string;
        userType?: string;
        isActive: boolean;
    };

    allUsers: any[];
}
