import {
  MatButtonModule, MatCheckboxModule, MatFormFieldModule,
  MatInputModule, MatDatepickerModule, MatTabsModule,
  MatCardModule, MatRadioModule, MatDialogModule,
  MatNativeDateModule, MatTableModule, MatPaginatorModule,
  MatIconModule, MatSelectModule, MatListModule
} from '@angular/material';
import { NgModule } from '@angular/core';

@NgModule({
  imports: [MatButtonModule, MatCheckboxModule,
    MatCardModule, MatRadioModule, MatDialogModule,
    MatTabsModule, MatDatepickerModule, MatNativeDateModule,
    MatTableModule, MatPaginatorModule, MatIconModule,
    MatSelectModule, MatListModule,
    MatFormFieldModule, MatInputModule],
  exports: [MatButtonModule, MatCheckboxModule,
    MatCardModule, MatRadioModule, MatDialogModule,
    MatTabsModule, MatDatepickerModule, MatNativeDateModule,
    MatTableModule, MatPaginatorModule, MatIconModule,
    MatSelectModule, MatListModule,
    MatFormFieldModule, MatInputModule],
})
export class CustomMaterialModule { }
