import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { RestService } from '../services/rest-service';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { ShowDialogComponent } from '../show-dialog/show-dialog.component';
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { State } from '../state/state';
import * as AppActions from '../state/actions';

@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.css'],
  providers: [RestService]
})
export class AdminPageComponent implements OnInit, OnDestroy {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  allUsers: any = [];
  newRequestDisplayedColumns: string[] = ['firstName', 'lastName', 'userType', 'userId'];
  newRequestDataSource: any;

  studentDisplayedColumns: string[] = ['firstName', 'lastName', 'dateOfBirth', 'ssn', 'createdTime', 'userId'];
  studentDataSource: any;

  professorDisplayedColumns: string[] = ['firstName', 'lastName', 'dateOfBirth', 'ssn', 'createdTime', 'userId'];
  professorDataSource: any;

  assignDisplayedColumns: string[] = ['firstName', 'lastName', 'subjects', 'professors', 'assign'];
  assignmentDataSource: any;

  gradeJson = { 'A++': 98, 'A+': 95, 'A': 90, 'B+': 80, 'B': 70, 'C': 60, 'D': 40 };

  subjects: any[] = [];
  professors: any[];

  subscription: Subscription = new Subscription();

  constructor(private service: RestService, private dialogRef: MatDialog, private store: Store<State>) {

  }

  ngOnInit() {
    this.store.dispatch(new AppActions.LoadAllUsers());
    this.subscription.add(this.store.select(st => st.allUsers).subscribe(data => {
      console.log('----data---', data);
      if (data && data.length > 0) {
        this.allUsers = data;
        const inActiveUsers = data.filter(u => u.active === false);
        const students = data.filter(u => u.active && u.userType === 'S');
        this.professors = data.filter(u => u.active && u.userType === 'P');
        const assignments = Object.assign([], students);
        assignments.forEach(s => { s.subjects = []; s.professors = []; s.assign = ''; })
        console.log('----data---', inActiveUsers, students, this.professors);

        this.newRequestDataSource = new MatTableDataSource<any>(inActiveUsers);
        this.studentDataSource = new MatTableDataSource<any>(students);
        this.assignmentDataSource = new MatTableDataSource<any>(assignments);
        this.professorDataSource = new MatTableDataSource<any>(this.professors);
        this.newRequestDataSource.paginator = this.paginator;
        this.studentDataSource.paginator = this.paginator;
        this.professorDataSource.paginator = this.paginator;
      }
    }, err => {
      this.dialogRef.open(ShowDialogComponent, { data: { text: err } });
    })

    );

    this.service.getCourses().subscribe(data => this.subjects = data ? data : []);

  }


  ngOnDestroy() {
    this.subscription.unsubscribe();
  }



  doApprove(userId) {
    console.log('----acivate button clicked----');
    this.service.activateUser(userId).subscribe(res => {
      this.store.dispatch(new AppActions.LoadAllUsers());
    }, err => {
      this.dialogRef.open(ShowDialogComponent, { data: { text: err } });
    });
  }

  doDelete(userId) {
    console.log('----delete button clicked----');
    this.service.deleteUser(userId).subscribe(res => {
      this.store.dispatch(new AppActions.LoadAllUsers());
    }, err => {
      this.dialogRef.open(ShowDialogComponent, { data: { text: err } });
    });
  }


  doAssign(element) {
    console.log('----element----', element);
    this.service.assignStudentCourses(element).subscribe(data => {
      console.log('--data--', data);
    });


  }


  getStudentName(assgn) {
    const user = this.allUsers.filter(u => u.userId === assgn.assignmentId.studentId)[0];
    return user.firstName + ' ' + user.lastName;
  }
  getCourseName(assgn) {
    const course = this.subjects.filter(u => u.courseId == assgn.assignmentId.courseId)[0];
    return course.courseName;
  }
  getProfessorName(assgn) {
    const user = this.allUsers.filter(u => u.userId === assgn.assignmentId.professorId)[0];
    return user.firstName + ' ' + user.lastName;
  }
  overallGrade(data) {
    const gradeCount = data.filter(o => o.grade).map(g => this.gradeJson[g.grade]).reduce((sum, current) => sum + current, 0);
    if (gradeCount > 0) {
      return Object.keys(this.gradeJson).filter(g => this.gradeJson[g] < gradeCount / data.length)[0];
    } else {
      return 'NA';
    }
  }

  studentRowClicked(row) {
    console.log('-----row---', row);
    this.service.getStudentAssignments(row.userId).subscribe(data => {
      let htmlText = [];
      htmlText.push(' Student Name :' + this.getStudentName(data[0]));
      htmlText.push(' Overall Grade :' + this.overallGrade(data));
      data.filter(o => o.assignmentId.assignmentSeqId == '0').forEach(o => {
        htmlText.push(' Subject :' + this.getCourseName(o) + '  Professor :' + this.getProfessorName(o));
      })
      this.dialogRef.open(ShowDialogComponent, { data: { texts: htmlText, title: 'Details' } });
    });
  }

}
