import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './home-page/home-page.component';
import { AdminPageComponent } from './admin-page/admin-page.component';
import { StudentPageComponent } from './student-page/student-page.component';
import { ProfessorPageComponent } from './professor-page/professor-page.component';
import { AlwaysAuthGuard } from './always-auth-guard';


const appRoutes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'home' },
  { path: 'home', pathMatch: 'full', component: HomePageComponent },
  { path: 'adminPage', pathMatch: 'full', component: AdminPageComponent, canActivate: [AlwaysAuthGuard], },
  { path: 'professorPage', pathMatch: 'full', component: ProfessorPageComponent, canActivate: [AlwaysAuthGuard], },
  { path: 'studentPage', pathMatch: 'full', component: StudentPageComponent, canActivate: [AlwaysAuthGuard], }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [
    RouterModule
  ]
})
export class RoutingModule {

}
