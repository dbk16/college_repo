import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from '../state/state';
import { Subscription } from 'rxjs';
import { RestService } from '../services/rest-service';
import * as AppActions from '../state/actions';

@Component({
  selector: 'app-professor-page',
  templateUrl: './professor-page.component.html',
  styleUrls: ['./professor-page.component.scss'],
  providers: [RestService]
})
export class ProfessorPageComponent implements OnInit {

  subscription: Subscription = new Subscription();
  userInfo: any;
  userName: string = '';
  selectedCourse: any = '';
  selectedStudent: any = '';
  assignmentObj: any;
  courseData: any;
  allUsers: any;
  applicationUrl = "";
  distinctCourses: any;
  gradeJson = { 'A++': 98, 'A+': 95, 'A': 90, 'B+': 80, 'B': 70, 'C': 60, 'D': 40 };

  constructor(private store: Store<State>, private service: RestService) { }


  ngOnInit() {
    this.applicationUrl = this.service.getApplicationUrl();
    this.store.dispatch(new AppActions.LoadAllUsers());
    this.subscription.add(this.store.select(st => st.userInfo).subscribe(data => {
      this.userInfo = data || {};
      console.log('--user info--', this.userInfo);
      this.getAssignments();
    })).add(this.service.getCourses().subscribe(courseData => {
      this.courseData = courseData;
    })).add(this.store.select(st => st.allUsers).subscribe(allUsers => {
      this.allUsers = allUsers;
    }));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getAssignments() {
    this.service.getProfessorAssignments(this.userInfo.userId).subscribe(dataObj => {
      console.log('-----get students data---', dataObj);
      this.assignmentObj = dataObj;
      this.distinctCourses = new Set(dataObj.map(o => o.assignmentId.courseId));
      this.selectedCourse = Array.from(this.distinctCourses)[0];
      this.getFirstStudent();
      console.log('--distinctCourses--', this.distinctCourses);
    })
  }

  getStudentName(studentId) {
    const user = this.allUsers.find(u => u.userId === studentId);
    return user.firstName + ' ' + user.lastName;
  }
  getCourseName(courseId) {
    return this.courseData.find(u => u.courseId == courseId).courseName;
  }
  uniqueStudents(assgnList, courseId) {
    return new Set(assgnList.filter(o => o.assignmentId.courseId == courseId).map(o => o.assignmentId.studentId));
  }
  getFirstStudent() {
    const uniqueStudents = new Set(this.assignmentObj.filter(o => o.assignmentId.courseId == this.selectedCourse).map(o => o.assignmentId.studentId));
    this.selectedStudent = Array.from(uniqueStudents)[0];
  }

  uniqueStudentAssignments(assgnList, courseId, studentId) {
    return assgnList.filter(o => o.assignmentId.courseId == courseId && o.assignmentId.studentId == studentId);
  }

  addNewAssignment() {
    const newAssignObj = {
      assignmentId: {
        assignmentSeqId: this.uniqueStudentAssignments(this.assignmentObj, this.selectedCourse, this.selectedStudent).length,
        studentId: this.selectedStudent,
        courseId: this.selectedCourse,
        professorId: this.userInfo.userId
      },
      lastModifiedTime: new Date().getTime()
    };
    this.assignmentObj.push(newAssignObj);
    this.service.addnewAssignment(newAssignObj).subscribe(data => {
      console.log('--addnewAssignment---', data);
    });
  }

  overallGrade() {
    const asignments = this.assignmentObj.filter(o => o.grade && o.assignmentId.courseId == this.selectedCourse && o.assignmentId.studentId == this.selectedStudent);
    const gradeCount = asignments.map(g => this.gradeJson[g.grade]).reduce((sum, current) => sum + current, 0);
    if (gradeCount > 0) {
      return Object.keys(this.gradeJson).filter(g => (gradeCount / asignments.length) - this.gradeJson[g] >= 0)[0];
    } else {
      return 'NA';
    }
  }

  onSubmit(event, assgn) {
    event.preventDefault();
    const id = assgn.assignmentId.studentId + '_' + assgn.assignmentId.courseId;
    const fileUpload = document.getElementById('singleFileUploadInput_' + id) as HTMLInputElement;
    var files = fileUpload.files;
    this.uploadSingleFile(files[0], assgn);
  }

  updateGrade(assgn) {
    assgn.grade = assgn.tempGrade;
    delete assgn.tempGrade;
    this.service.updateGrade(assgn).subscribe(data => {
      console.log('--upgrade grade ---', data);
    });
  }


  uploadSingleFile(file, assgn) {
    const _this = this;
    var formData = new FormData();
    formData.append("file", file);
    const obj = [assgn.assignmentId]
    formData.set("payload", JSON.stringify(obj));

    var xhr = new XMLHttpRequest();
    xhr.open("POST", this.service.getApplicationUrl() + "rest/prof/assignment");

    xhr.onload = function () {
      console.log(xhr.responseText);
      var response = JSON.parse(xhr.responseText);
      console.log('----response----', response);
      _this.getAssignments();
    }

    xhr.send(formData);
  }
}
