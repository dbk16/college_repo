-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: clg_database
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `assignment_details`
--

DROP TABLE IF EXISTS `assignment_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `assignment_details` (
  `assignment_seq_id` int(11) NOT NULL DEFAULT '0',
  `student_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `professor_id` int(11) NOT NULL,
  `student_assign_doc_id` int(11) DEFAULT NULL,
  `prof_assign_doc_id` int(11) DEFAULT NULL,
  `grade` varchar(10) DEFAULT NULL,
  `last_modified_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`student_id`,`course_id`,`professor_id`,`assignment_seq_id`),
  KEY `assignment_details_users_fk_1` (`professor_id`),
  KEY `assignment_details_course_fk` (`course_id`),
  CONSTRAINT `assignment_details_course_fk` FOREIGN KEY (`course_id`) REFERENCES `course` (`course_id`),
  CONSTRAINT `assignment_details_users_fk` FOREIGN KEY (`student_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `assignment_details_users_fk_1` FOREIGN KEY (`professor_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assignment_details`
--

LOCK TABLES `assignment_details` WRITE;
/*!40000 ALTER TABLE `assignment_details` DISABLE KEYS */;
INSERT INTO `assignment_details` VALUES (0,13,1,20,NULL,27,NULL,'2019-04-01 18:32:46'),(1,13,1,20,NULL,28,NULL,'2019-03-29 01:46:42'),(0,13,2,20,NULL,31,NULL,'2019-04-01 18:32:46'),(0,13,6,20,NULL,24,NULL,'2019-04-01 18:32:46'),(1,13,6,20,NULL,32,NULL,'2019-04-03 05:18:44'),(2,13,6,20,NULL,33,NULL,'2019-04-03 05:18:59'),(0,19,1,20,34,25,'A+','2019-03-29 01:46:42'),(1,19,1,20,34,29,'A++','2019-03-29 01:46:42'),(2,19,1,20,34,30,'A++',NULL),(0,19,2,20,22,20,'B','2019-03-29 01:46:42'),(0,19,3,20,23,21,'C','2019-03-29 01:46:43');
/*!40000 ALTER TABLE `assignment_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course`
--

DROP TABLE IF EXISTS `course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `course` (
  `course_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course`
--

LOCK TABLES `course` WRITE;
/*!40000 ALTER TABLE `course` DISABLE KEYS */;
INSERT INTO `course` VALUES (1,'JAVA'),(2,'ORACLE'),(3,'UI'),(4,'PHP'),(5,'Python'),(6,'AWS'),(7,'Apache'),(8,'Angular'),(9,'React');
/*!40000 ALTER TABLE `course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documents`
--

DROP TABLE IF EXISTS `documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `documents` (
  `document_id` int(10) NOT NULL AUTO_INCREMENT,
  `document_name` varchar(500) DEFAULT NULL,
  `uploaded_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`document_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documents`
--

LOCK TABLES `documents` WRITE;
/*!40000 ALTER TABLE `documents` DISABLE KEYS */;
INSERT INTO `documents` VALUES (1,'sweety verification.pdf','2019-03-27 19:26:45'),(2,'sweety verification.pdf','2019-03-27 19:31:06'),(3,'1553715270481_sweety verification.pdf','2019-03-27 19:34:31'),(4,'1553739081061_vijaya.pdf','2019-03-28 02:11:21'),(5,'1553739095565_balu.pdf','2019-03-28 02:11:36'),(6,'1553739101028_vijaya.pdf','2019-03-28 02:11:41'),(7,'1553743916017_sweety verification.pdf','2019-03-28 03:31:56'),(8,'1553744059298_vijaya.pdf','2019-03-28 03:34:19'),(9,'1553744321346_vijaya.pdf','2019-03-28 03:38:41'),(10,'1553744684676_sweety verification.pdf','2019-03-28 03:44:45'),(11,'1553745482853_balu.pdf','2019-03-28 03:58:03'),(12,'1553745483537_sweety verification.pdf','2019-03-28 03:58:04'),(13,'1553745571282_balu.pdf','2019-03-28 03:59:31'),(14,'1553745825368_balu.pdf','2019-03-28 04:03:45'),(15,'1553745830415_sweety verification.pdf','2019-03-28 04:03:50'),(16,'1553746499353_balu.pdf','2019-03-28 04:14:59'),(17,'1553746541785_balu.pdf','2019-03-28 04:15:42'),(18,'1553824918617_balu.pdf','2019-03-29 02:01:59'),(19,'1553825039883_vijaya.pdf','2019-03-29 02:04:00'),(20,'1553825057577_vijaya.pdf','2019-03-29 02:04:18'),(21,'1553825062405_vijaya.pdf','2019-03-29 02:04:22'),(22,'1553825202764_balu.pdf','2019-03-29 02:06:43'),(23,'1553825207048_sweety verification.pdf','2019-03-29 02:06:47'),(24,'1554265341602_vijaya.pdf','2019-04-03 04:22:22'),(25,'1554267461099_vijaya.pdf','2019-04-03 04:57:41'),(26,'1554267854907_vijaya.pdf','2019-04-03 05:04:15'),(27,'1554268403825_vijaya.pdf','2019-04-03 05:13:24'),(28,'1554268409780_vijaya.pdf','2019-04-03 05:13:30'),(29,'1554268528153_sweety verification.pdf','2019-04-03 05:15:28'),(30,'1554268626772_balu.pdf','2019-04-03 05:17:07'),(31,'1554268758999_sweety verification.pdf','2019-04-03 05:19:19'),(32,'1554268766099_vijaya.pdf','2019-04-03 05:19:26'),(33,'1554268769702_balu.pdf','2019-04-03 05:19:30'),(34,'1554272256150_balu.pdf','2019-04-03 06:17:36');
/*!40000 ALTER TABLE `documents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(100) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `is_active` bit(1) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `user_type` char(1) NOT NULL,
  `date_of_birth` date DEFAULT NULL,
  `ssn` varchar(100) DEFAULT NULL,
  `created_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','admin','admin','admin',_binary '','123@gmail.com','A',NULL,NULL,NULL),(2,'kdasari','krishna','dasari','[C@1215182c',_binary '','123@gmai1l.com','P','2019-03-06','21424325','2019-03-24 05:32:42'),(13,'tZZ1','test','ZZ','[C@3f4ac01d',_binary '','krishna.dasarin1@gmail.com','S','2019-03-06','121`2','2019-03-24 05:32:42'),(19,'ddasari1231','dasari','dasari123','[C@3e8f0b2d',_binary '','krishna.dasarin@gmail.com','S','2019-03-07','1234','2019-03-29 01:40:04'),(20,'AAnkeet1','Ankeet','Ankeet','[C@4022d1ac',_binary '','ankeetpatnaik10@gmail.com','P','2019-03-21','2131231','2019-03-29 01:40:56');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'clg_database'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-03 12:49:57
